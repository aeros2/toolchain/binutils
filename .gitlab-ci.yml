image: debian:testing-slim

variables:
  GIT_DEPTH: 1
  DEBIAN_FRONTEND: noninteractive
  PREFIX: /opt/i386-aeros

stages:
  - build
  - check
  - release
  - gcc

docker:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:v1.14.0-debug
    entrypoint: [""]
  before_script:
    - &VERSION
      export VERSION=${CI_COMMIT_TAG:-$(echo ${CI_COMMIT_REF_NAME} | sed 's/^aeros\///')-${CI_COMMIT_SHORT_SHA}}
    - &PACKAGE
      export PACKAGE="i386-aeros-binutils-${VERSION}"
  script:
    - /kaniko/executor
      --context "${CI_PROJECT_DIR}"
      --dockerfile "${CI_PROJECT_DIR}/Dockerfile"
      --destination "${CI_REGISTRY_IMAGE}:${VERSION}"
      --cache=true
  rules:
    - if: $CI_COMMIT_TAG
    - when: manual
      # allow_failure:true is required for subsequent jobs to run even if this has not been triggered
      allow_failure: true

build:
  stage: build
  variables:
    CCACHE_BASEDIR: ${CI_PROJECT_DIR}
    CCACHE_DIR: ${CI_PROJECT_DIR}/ccache
    CC: "ccache gcc"
    CXX: "ccache g++"
    CPP: "${CC} -E"
    CCACHE_COMPILERCHECK: content
    SYSROOT: "${CI_PROJECT_DIR}/sysroot"
  cache:
    paths:
      - ${CCACHE_DIR}
  before_script:
    - apt-get update -qq
    - apt-get install -qq --no-install-recommends curl ca-certificates ccache bison flex make libc6-dev libisl-dev gcc g++ texinfo
    - ccache --show-stats
    - ccache --zero-stats
    - *VERSION
    - *PACKAGE
  script:
    - export SOURCE_DATE_EPOCH=$(date -d "${CI_COMMIT_TIMESTAMP}" +%s)
    - mkdir build
    - (cd build && ../configure --target=i386-aeros --with-sysroot="${SYSROOT}" --prefix="${PREFIX}" --disable-werror --disable-nls --disable-gdb --disable-readline --disable-sim --enable-deterministic-archives)
    - make -C build
    - make -C build install-strip DESTDIR=${CI_PROJECT_DIR}/dist/${PACKAGE}
    - cd ${CI_PROJECT_DIR}/dist/${PACKAGE} && tar -czf ${CI_PROJECT_DIR}/dist/${PACKAGE}.tar.gz *
  after_script:
    - ccache --show-stats
  artifacts:
    paths:
      - ${CI_PROJECT_DIR}/dist/*.tar.gz

can-assemble:
  stage: check
  dependencies:
    - build
  before_script:
    - *VERSION
    - *PACKAGE
    - export PATH="${PREFIX}/bin:${PATH}"
    - tar -C / -xf ${CI_PROJECT_DIR}/dist/${PACKAGE}.tar.gz
  script:
    - ls -l ${PREFIX}/bin
    - command -v i386-aeros-as
    - echo 'movb $0x05, %al' | i386-aeros-as -al

release:
  stage: release
  dependencies:
    - build
  before_script:
    - *VERSION
    - *PACKAGE
  script:
    - 'curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file dist/${PACKAGE}.tar.gz "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/i386-aeros-binutils/${CI_COMMIT_REF_NAME}/${PACKAGE}.tar.gz"'
  rules:
    - if: $CI_COMMIT_TAG

trigger-gcc:
  stage: gcc
  allow_failure: true
  variables:
    BINUTILS_VERSION: ${CI_COMMIT_REF_NAME}
    BINUTILS_PROJECT: ${CI_PROJECT_ID}
  trigger: aeros2/toolchain/gcc
  rules:
    - when: manual
