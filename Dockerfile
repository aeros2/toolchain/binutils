FROM debian:testing-slim AS builder

RUN rm -f /etc/apt/apt.conf.d/docker-clean
RUN --mount=target=/var/lib/apt/lists,type=cache,sharing=locked \
    --mount=target=/var/cache/apt,type=cache,sharing=locked \
    apt-get update \
    && apt-get install -yy gcc g++ make bison flex libgmp3-dev libmpfr-dev zlib1g-dev texinfo

RUN mkdir /prefix
WORKDIR /build

COPY . /src
COPY sysroot /sysroot
RUN /src/configure --target=i386-aeros --with-sysroot=/sysroot --prefix=/prefix --disable-nls --disable-werror --with-system-zlib --without-gdb
RUN make -j `nproc`
RUN make install-strip

RUN ls -l /prefix/i386-aeros/bin

FROM debian:testing-slim

COPY --from=builder /prefix /opt/i386-aeros
COPY --from=builder /sysroot /sysroot

RUN ls -l /opt/i386-aeros/i386-aeros/bin

ENV PATH="${PATH}:/opt/i386-aeros/bin"
